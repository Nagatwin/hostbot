import datetime
import io
import logging
import os
import pickle

import dateutil.parser
import discord
import pytz
from PIL import Image, ImageDraw, ImageFont
from discord.ext import tasks
from dotenv import load_dotenv
from tabulate import tabulate

from tzinfos_compl import timezone_info

KNOWN_TIMEZONES = [tz.lower() for tz in list(timezone_info.keys())]

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
MASTER = os.getenv('DISCORD_MASTER')
AUTH_GUILDS_FILE = os.getenv('AUTH_GUILDS_FILE')

AUTH_GUILDS = set()


def save_auth_guilds():
    with open(AUTH_GUILDS_FILE, 'wb') as f:
        pickle.dump(AUTH_GUILDS, f)


# create logger
logger = logging.getLogger("discord_bot")
logger.setLevel(logging.DEBUG)

# create console handler and set level to debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")

# add formatter to ch
ch.setFormatter(formatter)

# add ch to logger
logger.addHandler(ch)


def log(logging_level, msg):
    logging_level(msg)


def get_last_utc(utc_hour=7):
    now = datetime.datetime.now(pytz.utc)
    if now.hour < utc_hour:
        now = now - datetime.timedelta(days=1)
    return now.replace(hour=utc_hour, minute=0, second=0, microsecond=0)


def get_last_utc_monday(utc_hour=7):
    now = get_last_utc(utc_hour)
    while now.weekday() != 0:
        now = now - datetime.timedelta(days=1)
    return now


def get_nick_if_any(member):
    if member.nick is None:
        return member.name
    else:
        return member.nick


def get_handle(member):
    return member.name + '#' + member.discriminator


def text_to_image(text):
    fnt = ImageFont.truetype('RobotoMono-Light.ttf', 30)
    img = Image.new('RGB', (1, 1))
    d = ImageDraw.Draw(img)

    img = Image.new('RGB', d.textsize(text, font=fnt), color=(255, 255, 255))
    d = ImageDraw.Draw(img)

    d.text((0, 0), text, fill=(0, 0, 0), font=fnt)
    arr = io.BytesIO()
    img.save(arr, format='png')
    arr.seek(0)
    imgFile = discord.File(arr, "image.png")
    return imgFile


def table_to_image(table, headers):
    te = tabulate(table, headers=headers, tablefmt='orgtbl') + '\n'
    return text_to_image(te)


def tables_to_image(tables, headerss):
    te = []
    for i in range(len(tables)):
        if i == 0 and len(tables[i]) == 0:
            continue
        te.append(tabulate(tables[i], headers=headerss[i], tablefmt='orgtbl') + '\n')
    return text_to_image('\n'.join(te))


async def try_delete(message):
    try:
        await message.delete()
    except:
        # % No permission
        log(logger.warning, 'No permission to delete message')


if not os.path.exists('./save'):
    log(logger.warning, 'save dir not found, creating it')
    os.mkdir('./save')

try:
    if os.path.exists(AUTH_GUILDS_FILE):
        log(logger.info, 'Reading authorised guilds')
        with open(AUTH_GUILDS_FILE, 'rb') as f:
            AUTH_GUILDS = pickle.load(f)
except:
    log(logger.warning, 'Could not read authorized guilds file')
    AUTH_GUILDS = set()
    save_auth_guilds()


class Event():
    def __init__(self, event_id, host_id, time, timezone, event_type, co_host):
        self.event_id = event_id
        self.time = time
        self.timezone = timezone
        self.host_id = host_id
        self.co_host = co_host
        self.event_type = event_type

    def check_integrity(self, event_hanlder):
        if 'event_type' not in self.__dict__.keys():
            log(logger.warning, 'Event type not found, assuming NA')
            self.event_type = 'NA'
        if 'event_id' not in self.__dict__.keys():
            log(logger.warning, 'Event id not found, computing one')
            self.event_id = event_hanlder.get_next_event_id()
        if 'co_host' not in self.__dict__.keys():
            log(logger.warning, 'Event co_host not found, assuming None')
            self.co_host = None
        self.event_type = self.event_type.upper()


class CantParseDateException(Exception):
    pass


class NoTimezoneException(Exception):
    pass


class EventOverlapException(Exception):
    pass


class NoEventTypeException(Exception):
    pass


class NoAmPmTimeException(Exception):
    pass


class EventsHandler():
    def __init__(self, guild_id, save_file):
        self.guild_id = guild_id
        self.channel = ''
        self.mod_channel = ''
        self.events = set()
        self.save_file = save_file
        self.logs_channel = ''
        self.last_event_id = 0

    def get_next_event_id(self):
        self.last_event_id += 1
        return self.last_event_id

    def add_event(self, host_id, type, event_time, check_time_24h=True):
        """Add an event for host at event_time"""
        time_zones = [value for value in event_time.split() if value.lower() in KNOWN_TIMEZONES]

        # % Can't find timezone
        if len(time_zones) != 1:
            raise NoTimezoneException

        time_zone = time_zones[0]

        if type.upper() not in ['C', 'M', 'S', 'P', 'N']:
            raise NoEventTypeException

        if 'am' not in event_time.lower() and 'pm' not in event_time.lower():
            raise NoAmPmTimeException

        time = None
        # % Parse time
        try:
            time = dateutil.parser.parse(event_time, tzinfos=timezone_info)
        except:
            raise CantParseDateException

        if check_time_24h:
            # % Make event future in less than 25h
            while time.astimezone(pytz.utc) < datetime.datetime.now(pytz.utc):
                time = time + datetime.timedelta(hours=24)
            while time.astimezone(pytz.utc) > datetime.datetime.now(pytz.utc) + datetime.timedelta(hours=24):
                time = time - datetime.timedelta(hours=24)

        for event in self.events:
            if event.time.astimezone(pytz.utc) == time.astimezone(pytz.utc):
                log(logger.warning, 'OVERLAP EXCEPTION {} {}'.format(event.__dict__, time.astimezone(pytz.utc)))
                raise EventOverlapException

        log(logger.info,
            'Adding event for {} with time {}, timezone {}, type {}'.format(host_id, time, time_zone, type))

        self.events.add(Event(self.get_next_event_id(), host_id, time, time_zone, type.upper(), None))
        self.save()

    def get_last_week_events(self):
        monday = get_last_utc_monday() - datetime.timedelta(days=7)
        week_events = [e for e in self.events if e.time.astimezone(pytz.utc) > monday.astimezone(pytz.utc)]
        monday_next = monday + datetime.timedelta(days=7)
        week_events = [e for e in week_events if e.time.astimezone(pytz.utc) < monday_next]
        return week_events

    def remove_events(self, host_id):
        """Remove all future events of host_id"""
        new_set = set()
        for e in self.events:
            if e.time.astimezone(pytz.utc) > datetime.datetime.now(pytz.utc) and e.host_id == host_id:
                continue

            new_set.add(e)

        self.events = new_set
        self.save()
        return True

    def set_active_channel(self, channel):
        self.channel = channel
        self.save()

    def set_active_mod_channel(self, channel):
        self.mod_channel = channel
        self.save()

    def set_active_logs_channel(self, channel):
        self.logs_channel = channel
        self.save()

    def todays_events(self, prv_days=0):
        selected_events = []
        for e in self.events:
            if e.time.astimezone(pytz.utc) < get_last_utc() - datetime.timedelta(days=prv_days):
                continue
            selected_events.append(e)
        selected_events = sorted(selected_events, key=lambda e: e.time.astimezone(pytz.utc))
        return selected_events

    def save(self):
        log(logger.info, 'Saving event handler for guild {}'.format(self.guild_id))
        with open(self.save_file, 'wb') as f:
            pickle.dump(self, f)

    def check_integrity(self):
        if 'last_event_id' not in self.__dict__.keys():
            self.last_event_id = 0
        for e in self.events:
            e.check_integrity(self)
        if 'logs_channel' not in self.__dict__.keys():
            self.logs_channel = ''
        if 'mod_channel' not in self.__dict__.keys():
            self.mod_channel = ''
        if not isinstance(self.events, set):
            self.events = set(self.events)


class ThemeNotFoundException(Exception):
    pass


class ThemeFullException(Exception):
    pass


class SuperSaturdayHandler():
    def __init__(self, guild_id, save_file):
        self.guild_id = guild_id
        self.channel = ''
        self.themes = set()
        self.claims = {}
        self.save_file = save_file
        self.wildcards = {}

    def reset(self):
        self.themes = set()
        self.save()
        self.claims = {}
        self.wildcards = {}

    def add_theme(self, theme):
        self.themes.add(theme.lower())
        self.save()

    def remove_theme(self, theme):
        if theme.lower() not in self.themes:
            raise ThemeNotFoundException()
        self.themes.remove(theme.lower())
        self.save()

    def set_wildcard(self, theme, host_nick, host_id):
        if theme in self.wildcards.keys() or theme in self.themes:
            raise ThemeFullException

        # to_delete = []
        # for theme_key in self.wildcards.keys():
        #    if self.wildcards[theme_key]['id'] == host_id:
        #        to_delete.append(theme_key)

        # for theme_key in to_delete:
        #    del self.wildcards[theme_key]

        self.wildcards[theme.lower()] = {'id': host_id, 'nick': host_nick}
        self.save()

    def set_active_channel(self, channel):
        self.channel = channel
        self.save()

    def claim_theme(self, theme, host_nick, host_id):
        theme = theme.lower()
        if theme not in self.themes:
            raise ThemeNotFoundException
        if theme not in self.claims.keys():
            self.claims[theme] = []
        if len(self.claims[theme]) >= 2:
            raise ThemeFullException
        self.claims[theme].append({'nick': host_nick, 'id': host_id})
        self.save()

    def unclaim_theme(self, theme, host_id):
        theme = theme.lower()
        if theme not in self.claims.keys() and theme not in self.wildcards.keys():
            raise ThemeNotFoundException
        if theme in self.claims.keys():
            self.claims[theme] = [h for h in self.claims[theme] if h['id'] != host_id]
        if theme in self.wildcards.keys():
            if self.wildcards[theme]['id'] == host_id:
                del self.wildcards[theme]
        self.save()

    def get_current_themes(self):
        return '**Current themes** :\n{}'.format('\n'.join(self.themes))

    def save(self):
        log(logger.info, 'Saving Super Saturday Handler for guild {}'.format(self.guild_id))
        with open(self.save_file, 'wb') as f:
            pickle.dump(self, f)

    def check_integrity(self):
        if 'wildcards' not in self.__dict__.keys():
            self.wildcards = {}
        return


class ACNHDiscordHostBot(discord.Client):
    def __init__(self, intents):
        super().__init__(intents=intents)
        self.events_handler = {}
        self.super_saturdays_handler = {}
        self.last_table_handler = {}
        self.last_theme_handler = {}
        self.last_theme_table_handler = {}
        self.event_loop.start()

    def get_event_handler(self, guild_id):
        if not guild_id in self.events_handler.keys():
            save_file = os.path.join('./save', '{}_eventhandler.dat'.format(guild_id))
            if not os.path.exists(save_file):
                log(logger.warning, 'guild {} save not found, not loading'.format(guild_id))
                self.events_handler[guild_id] = EventsHandler(guild_id, save_file)
            else:
                log(logger.info, 'Loading guild {} save'.format(guild_id))

                with open(save_file, 'rb') as f:
                    self.events_handler[guild_id] = pickle.load(f)
                    self.events_handler[guild_id].check_integrity()

        self.events_handler[guild_id].save()

        return self.events_handler[guild_id]

    def get_super_saturdays_handler(self, guild_id):
        if not guild_id in self.super_saturdays_handler.keys():
            save_file = os.path.join('./save', '{}_supersaturdayshandler.dat'.format(guild_id))
            if not os.path.exists(save_file):
                log(logger.warning, 'guild {} save not found, not loading'.format(guild_id))
                self.super_saturdays_handler[guild_id] = SuperSaturdayHandler(guild_id, save_file)
            else:
                log(logger.info, 'Loading guild {} save'.format(guild_id))

                with open(save_file, 'rb') as f:
                    self.super_saturdays_handler[guild_id] = pickle.load(f)
                    self.super_saturdays_handler[guild_id].check_integrity()

        self.super_saturdays_handler[guild_id].save()
        return self.super_saturdays_handler[guild_id]

    async def on_ready(self):
        await self.change_presence(activity=discord.Game(name='Potato Contest'))
        log(logger.info, 'Server is now ready.')
        for guild in self.guilds:
            self.events_handler[guild.id] = self.get_event_handler(guild.id)
            self.last_table_handler[guild.id] = None
            self.last_theme_handler[guild.id] = None
            self.last_theme_table_handler[guild.id] = None
            log(logger.info, '{} is connected to the guild: {} (id: {})'.format(self.user, guild.name, guild.id))
            for member in guild.members:
                if str(member.id) == str(MASTER):
                    await member.create_dm()
                    await member.dm_channel.send(
                        "Hey, I'm back on this server : {} (id: {})!".format(guild.name, guild.id))
            with open('{}.csv'.format(guild.id), 'w') as f:
                host_count = {}
                for event in self.events_handler[guild.id].events:
                    if event.host_id not in host_count.keys():
                        host_count[event.host_id] = 0
                    host_count[event.host_id] += 1
                host_list = []
                for k in host_count.keys():
                    member = None

                    try:
                        member = await guild.fetch_member(k)
                    except Exception as e:
                        log(logger.info, 'Error fetching {}'.format(e))
                    if member is None:
                        host_list.append([k, '?', '?', host_count[k]])
                    else:
                        host_list.append([k, member.nick, member.name, host_count[k]])
                for l in host_list:
                    f.write('\t'.join([str(e) for e in l]) + '\n')

        # await self.send_weekly_update()

    async def on_guild_join(self, guild):
        self.events_handler[guild.id] = self.get_event_handler(guild.id)
        self.last_table_handler[guild.id] = None
        self.last_theme_handler[guild.id] = None
        self.last_theme_table_handler[guild.id] = None
        log(logger.info, '{} is connected to the guild: {} (id: {})'.format(self.user, guild.name, guild.id))
        for member in guild.members:
            if str(member.id) == str(MASTER):
                await member.create_dm()
                await member.dm_channel.send("Hey, I joined this server : {} (id: {})!".format(guild.name, guild.id))

    async def poll(self, question, options, channel):
        if len(options) <= 1:
            await channel.send('You need more than one option to make a poll!')
            return

        parsed_options = []
        for option in options:
            if option == '':
                continue
            split = option.split()
            if len(split) < 2:
                await channel.send(
                    'The option ({}) should have the following format : "emoji Description of the option."'.format(
                        option))
                return
            emote = split[0]
            text = ' '.join(split[1:])
            parsed_options.append({'reaction': emote, 'text': text})

        description = []
        for option in parsed_options:
            description += '\n {} {}'.format(option['reaction'], option['text'])

        embed = discord.Embed(title=question, description=''.join(description))
        react_message = await channel.send(embed=embed)

        current_react = 0
        while current_react < len(parsed_options):
            if current_react % 20 == 0 and current_react != 0:
                react_message = await channel.send("Additional reactions")
            option = parsed_options[current_react]
            try:
                await react_message.add_reaction(option['reaction'])
            except:
                await channel.send(
                    "Error when adding reaction {} for option {}, use only emojis please".format(option['reaction'],
                                                                                                 option['text']))
            current_react += 1

    async def handle_message(self, message):
        global AUTH_GUILDS
        content = message.content
        while content.startswith('! '):
            content = content[0] + content[2:]
        command = content.split()[0]
        args = content.split()[1:]

        if str(message.author.id) == str(MASTER):
            # % Admin panel
            if command == '!auth':
                if message.guild.id not in AUTH_GUILDS:
                    AUTH_GUILDS.add(message.guild.id)
                    save_auth_guilds()
                    await message.channel.send("I'm now authorized on this server.")
                else:
                    await message.channel.send("I'm already authorized on this server.")
                return
            if command == '!deauth':
                AUTH_GUILDS.discard(message.guild.id)
                save_auth_guilds()
                await message.channel.send("I'm no longer authorized on this server.")
                return
            if command == '!sayhi':
                await message.channel.send("Hi :D")
                return

        if message.guild.id not in AUTH_GUILDS:
            return

        event_handler = self.get_event_handler(message.guild.id)
        super_saturdays_handler = self.get_super_saturdays_handler(message.guild.id)

        if str(message.author.id) == str(MASTER):
            # % Admin panel
            if command == '!activate_channel':
                event_handler.set_active_channel(' '.join(args))
                await message.channel.send("I'm now active on the #{} channel.".format(' '.join(args)))
                return
            if command == '!activate_mod_channel':
                event_handler.set_active_mod_channel(' '.join(args))
                await message.channel.send("I'm now active on the #{} channel for mods.".format(' '.join(args)))
                return
            if command == '!activate_supersaturdays_channel':
                super_saturdays_handler.set_active_channel(' '.join(args))
                await message.channel.send(
                    "I'm now active on the #{} channel for super saturdays.".format(' '.join(args)))
                return
            if command == '!activate_logs_channel':
                event_handler.set_active_logs_channel(' '.join(args))
                await message.channel.send("I'm now active on the #{} channel for daily logs".format(' '.join(args)))
                await self.send_daily_logs()
                return

        if message.channel.name == event_handler.channel:
            if command == '!host':
                try:
                    event_handler.add_event(message.author.id, args[0], ' '.join(args[1:]))
                except CantParseDateException:
                    log(logger.warning, 'Unhandled host command, wrong format : {}'.format(message.content))
                    await message.channel.send(
                        'Wrong format ({}), please try again.\nExample : !host C 6:00AM CEST'.format(message.content))
                    return
                except NoTimezoneException:
                    log(logger.warning, 'Unhandled host command, no timezone : {}'.format(message.content))
                    await message.channel.send(
                        "Wrong format ({}), please try again. Don't forget the timezone\nExample : !host C 6:00AM CEST".format(
                            message.content))
                    return
                except NoEventTypeException:
                    log(logger.warning, 'Unhandled host command, no event type : {}'.format(message.content))
                    await message.channel.send(
                        "Wrong format ({}), please try again. Don't forget the event type\nExample : !host C 6:00AM CEST".format(
                            message.content))
                    return
                except EventOverlapException:
                    log(logger.warning, 'Unhandled host command, event overlap : {}'.format(message.content))
                    await message.channel.send(
                        "Event overlap ({}), please use another time slot !".format(message.content))
                    return
                except NoAmPmTimeException:
                    log(logger.warning, 'Unhandled host command, event needs am/pm : {}'.format(message.content))
                    await message.channel.send(
                        "Event time needs am/pm ({}), please add am/pm info !".format(message.content))
                    return

            if command == '!dehost':
                if not event_handler.remove_events(message.author.id):
                    log(logger.warning, 'Unhandled dehost command : {}'.format(message.content))
                    await message.channel.send('Wrong format, please try again.\nExample : !dehost')
                    return

            if command == '!dehost' or command == '!host':
                events = event_handler.todays_events()

                tab = []
                for e in events:
                    member = await message.guild.fetch_member(e.host_id)
                    co_host_member_name = '-'
                    if e.co_host is not None:
                        co_host_member = await message.guild.fetch_member(e.co_host)
                        co_host_member_name = get_nick_if_any(co_host_member)
                    tab.append([e.event_id, get_nick_if_any(member), co_host_member_name, e.event_type,
                                e.time.strftime("%I:%M%p"), e.timezone,
                                e.time.astimezone(pytz.utc).strftime("%I:%M%p")])
                    # if e.host_id in host_nick.keys():
                    #    tab.append([host_nick[e.host_id], e.event_type, e.time.strftime("%I:%M%p"), e.timezone, e.time.astimezone(pytz.utc).strftime("%I:%M%p")])
                    # else:
                    #    tab.append(['ID ' + str(e.host_id), e.event_type, e.time.strftime("%I:%M%p"), e.timezone, e.time.astimezone(pytz.utc).strftime("%I:%M%p")])

                await message.channel.send('Events scheduled from 7AM UTC', file=table_to_image(tab,
                                                                                                headers=['Id', 'Host',
                                                                                                         'Co-Host',
                                                                                                         'Type', 'Time',
                                                                                                         'Zone',
                                                                                                         'UTC']))
                await try_delete(message)

        if 'Modmins' in [r.name for r in message.author.roles]:
            if command == '!hostpoll':
                vals = message.content.split('\n')
                await self.poll(vals[0][9:], vals[1:], message.channel)

        if message.channel.name == super_saturdays_handler.channel:
            if 'Modmins' in [r.name for r in message.author.roles] or 'Host Manager' in [r.name for r in
                                                                                         message.author.roles]:
                if command == '!theme':
                    if ' '.join(args) == '':
                        await message.channel.send("Cannot add an empty theme.")
                        return
                    super_saturdays_handler.add_theme(' '.join(args))
                    await message.channel.send(super_saturdays_handler.get_current_themes())
                    await try_delete(message)
                if command == '!resetthemes':
                    super_saturdays_handler.reset()
                    self.last_theme_handler[message.guild.id] = None
                    await message.channel.send('Reset done.')
                    await try_delete(message)
                if command == '!removetheme':
                    try:
                        super_saturdays_handler.remove_theme(' '.join(args))
                        await message.channel.send(super_saturdays_handler.get_current_themes())
                    except ThemeNotFoundException:
                        log(logger.warning, 'Unhandled mod command, theme not found : {}'.format(message.content))
                        await message.channel.send(
                            "Theme not found ({}). Please use the !removetheme command on existing theme **full** name".format(
                                message.content))
                        return
                    await try_delete(message)
            if command == '!claim':
                try:
                    super_saturdays_handler.claim_theme(' '.join(args), get_nick_if_any(message.author),
                                                        message.author.id)
                except ThemeNotFoundException:
                    log(logger.warning, 'Unhandled host command, theme not found : {}'.format(message.content))
                    await message.channel.send(
                        "Theme not found ({}). Please use the !claim command on existing theme **full** name".format(
                            message.content))
                    return
                except ThemeFullException:
                    log(logger.warning, 'Unhandled host command, theme full : {}'.format(message.content))
                    await message.channel.send(
                        "Theme is full ({}). 2 hosts already registered for this theme".format(message.content))
                    return
                await try_delete(message)
            if command == '!wildcard':
                try:
                    if ' '.join(args) == '':
                        await message.channel.send("Cannot add an empty theme.")
                        return
                    super_saturdays_handler.set_wildcard(' '.join(args), get_nick_if_any(message.author),
                                                         message.author.id)
                except ThemeFullException:
                    log(logger.warning, 'Unhandled host command, theme full : {}'.format(message.content))
                    await message.channel.send(
                        "Theme is full ({}). This theme has already been registered".format(message.content))
                    return
                await try_delete(message)
            if command == '!unclaim':
                try:
                    super_saturdays_handler.unclaim_theme(' '.join(args), message.author.id)
                except ThemeNotFoundException:
                    log(logger.warning, 'Unhandled host command, theme not found : {}'.format(message.content))
                    await message.channel.send(
                        "Theme not found ({}). Please use the !unclaim command on existing theme **full** name".format(
                            message.content))
                    return
                await try_delete(message)
            if command == '!unclaim' or command == '!claim' or command == '!wildcard':
                tab = []
                for theme in super_saturdays_handler.themes:
                    theme_row = [theme]
                    if theme in super_saturdays_handler.claims.keys():
                        theme_row += [h['nick'] for h in super_saturdays_handler.claims[theme]]
                    while len(theme_row) < 3:
                        theme_row.append('-')
                    tab.append(theme_row)

                tab2 = []
                for theme in super_saturdays_handler.wildcards.keys():
                    tab2.append([theme, super_saturdays_handler.wildcards[theme]['nick']])

                img = tables_to_image([tab, tab2], [['Theme', 'Host 1', 'Host 2'], ['Wildcard', 'host']])

                await message.channel.send('Themes schedule', file=img)

    async def send_weekly_update(self):
        for guild in self.guilds:
            log(logger.debug, 'guild {}'.format(guild.name))
            event_handler = self.get_event_handler(guild.id)

            host_count = {}
            async for member in guild.fetch_members(limit=None):
                if 'Hosts' in [r.name for r in member.roles] or 'Host Manager' in [r.name for r in member.roles]:
                    host_count[member.id] = 0

            week_events = event_handler.get_last_week_events()
            for e in week_events:
                if e.host_id not in host_count:
                    host_count[e.host_id] = 0
                host_count[e.host_id] += 1

            total = len(week_events)
            host_count_list = []
            for member_id in host_count.keys():
                try:
                    member = await guild.fetch_member(member_id)
                    host_count_list.append([get_nick_if_any(member), host_count[member_id]])
                except:
                    log(logger.info,
                        "Can't retrieve name of {} that hosted {} events".format(member_id, host_count[member_id]))

            host_count_list = sorted(host_count_list, key=lambda e: e[1])[::-1]

            mvps_text = ''

            for channel in guild.channels:
                log(logger.debug, 'channel {}'.format(channel.name))
                if channel.name == event_handler.mod_channel:
                    log(logger.debug, 'Sending mod message')
                    await channel.send(
                        'Week of {}:'.format((datetime.datetime.now() - datetime.timedelta(days=1)).strftime("%D")),
                        file=table_to_image(host_count_list, headers=['Host', 'Events this week']))
                if channel.name == event_handler.channel:
                    log(logger.debug, 'Sending host message')
                    await channel.send(
                        'Such a nice week !\nAll in all we hosted {} events :confetti_ball: !'.format(total))

    async def check_event_quota(self):
        for guild in self.guilds:
            log(logger.debug, 'guild {}'.format(guild.name))
            event_handler = self.get_event_handler(guild.id)
            events = event_handler.todays_events()

            now = datetime.datetime.now(pytz.utc)
            total_events_needed = (3 if (now.weekday() != 6 and now.weekday() != 5) else 5)
            if len(events) <= total_events_needed - 2:
                for channel in guild.channels:
                    log(logger.debug, 'channel {}'.format(channel.name))
                    if channel.name == event_handler.channel:
                        log(logger.debug, 'Sending host message')
                        await channel.send(
                            'Hello there ! We are {} events away to make it to {} events today ! Keep up the good work!'.format(
                                total_events_needed - len(events), total_events_needed))

    async def send_daily_logs(self):
        for guild in self.guilds:
            log(logger.debug, 'guild {}'.format(guild.name))
            event_handler = self.get_event_handler(guild.id)
            events = event_handler.todays_events()
            now = datetime.datetime.now(pytz.utc)
            total_events = len(events)
            for channel in guild.channels:
                log(logger.debug, 'channel {}'.format(channel.name))
                if channel.name == event_handler.logs_channel:
                    log(logger.debug, 'Sending daily message')
                    await channel.send('Events hosted today: {}'.format(total_events))

    @tasks.loop(minutes=1.0)
    async def event_loop(self):
        now = datetime.datetime.now(pytz.utc)
        if now.hour == 7 and now.minute == 0 and now.weekday() == 0:
            await self.send_weekly_update()
        if now.minute == 59 and now.hour == 6:
            await self.send_daily_logs()
        if now.hour == 22 and now.minute == 0:
            await self.check_event_quota()

    async def on_message(self, message):
        if message.author == self.user:
            if message.guild is not None and message.guild.id in self.last_table_handler.keys():
                if message.content.startswith('Events scheduled from'):
                    if self.last_table_handler[message.guild.id] is not None and \
                            self.last_table_handler[message.guild.id]['time'] == get_last_utc():
                        await try_delete(self.last_table_handler[message.guild.id]['message'])
                    self.last_table_handler[message.guild.id] = {'message': message, 'time': get_last_utc()}
            if message.guild is not None and message.guild.id in self.last_theme_handler.keys():
                if message.content.startswith('**Current themes** :'):
                    if self.last_theme_handler[message.guild.id] is not None:
                        await try_delete(self.last_theme_handler[message.guild.id]['message'])
                    self.last_theme_handler[message.guild.id] = {'message': message}
            if message.guild is not None and message.guild.id in self.last_theme_table_handler.keys():
                if message.content.startswith('Themes schedule'):
                    if self.last_theme_table_handler[message.guild.id] is not None:
                        await try_delete(self.last_theme_table_handler[message.guild.id]['message'])
                    self.last_theme_table_handler[message.guild.id] = {'message': message}
            return

        if message.guild is None:
            await message.channel.send("Hello, this bot is not meant to be used in DMs.")
            return

        if message.content == '':
            return
        log(logger.debug, 'New message from {} received : \'{}\''.format(get_handle(message.author), message.content))

        await self.handle_message(message)

    async def on_error(self, event, *args, **kwargs):
        with open('err.log', 'a') as f:
            if event == 'on_message':
                f.write('Unhandled message: ' + str(args) + '\n')
                raise
            else:
                raise


intents = discord.Intents.default()
intents.members = True
client = ACNHDiscordHostBot(intents=intents)

client.run(TOKEN)
